varying vec2 vUv ;
uniform float uProgress;
uniform vec2 uSizeDiv;
uniform vec2 uSize;



void main()

{
 
    float ratioIdeal  = uSize.x/uSize.y;
    float ratio  = uSizeDiv.x/uSizeDiv.y;
    if(ratio<ratioIdeal){
        float facteur = (uSizeDiv.x*uSize.y)/(uSizeDiv.y*uSize.x);
        float x=facteur*(uv.x-0.5)+0.5;
        vUv = vec2(x,uv.y);
    }else{
               float facteur = (uSizeDiv.y*uSize.x)/(uSizeDiv.x*uSize.y);
        float y=facteur*(uv.y-0.5)+0.5;
        vUv = vec2(uv.x,y);
    }
    
    

 
    gl_Position = projectionMatrix * modelViewMatrix * vec4(position, 1.0);
}