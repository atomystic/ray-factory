varying vec2 vUv;
uniform sampler2D uTexture;
uniform vec3 uVoile;



void main()
{
    vec4 Target = texture2D(uTexture, vUv);
    vec4 Blend  = vec4(uVoile,1.0);
    //Darken
    //  vec4 finalColor = min(Target,Blend) ;

    //Multiply
    // vec4 finalColor = Target * Blend ;

    //Color Burn 
    // vec4 finalColor = 1.0 - (1.0-Target) / Blend;

    //Linear Burn 
    // vec4 finalColor =Target + Blend - 1.0;

    //Lighten 
    // vec4 finalColor =max(Target,Blend);

    //Screen 
    // vec4 finalColor =1.0 - (1.0-Target) * (1.0-Blend);

    //Color Dodge
    // vec4 finalColor =Target / (1.0-Blend);

    //Linear Dodge
    // vec4 finalColor =Target + Blend;

    //Overlay
    // vec4 finalColor =step(Target,vec4(vec3(0.5),1.0)) * (1.0 - (1.0-2.0*(Target-0.5)) * (1.0-Blend)) + step(vec4(vec3(0.5),1.0),Target) * ((2.0*Target) * Blend);

    //Soft Light
    // vec4 finalColor =step(Blend,vec4(vec3(0.5),1.0)) * (1.0 - (1.0-Target) * (1.0-(Blend-0.5))) +step(vec4(vec3(0.5),1.0),Blend) * (Target * (Blend+0.5));

    //Exclusion
    // vec4 finalColor =0.5 - 2.0*(Target-0.5)*(Blend-0.5);

    //Difference	
    // vec4 finalColor = abs(Target - Blend);
        

     gl_FragColor = vec4(Target);
}