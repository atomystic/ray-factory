import './style.css'
import { THREE } from "./Common"

import { OrbitControls } from 'three/examples/jsm/controls/OrbitControls.js'
import * as dat from 'dat.gui'
// import testVertexShader from './shaders/test/vertex.glsl'
// import testFragmentShader from './shaders/test/fragment.glsl'
import { PoolObject } from './PoolObject'

import { EffectComposer } from 'three/examples/jsm/postprocessing/EffectComposer';
import { RenderPass } from 'three/examples/jsm/postprocessing/RenderPass.js';
import { UnrealBloomPass } from 'three/examples/jsm/postprocessing/UnrealBloomPass.js';

import Stats from "stats.js";


// Debug
const gui = new dat.GUI()
const params = {
    exposure: 1,
    bloomStrength: 1.5,
    bloomThreshold: 0,
    bloomRadius: 0
};

// Stats
var stats = new Stats();
stats.showPanel(0); // 0: fps, 1: ms, 2: mb, 3+: custom
document.body.appendChild(stats.dom);

// Canvas
const canvas = document.querySelector('canvas.webgl')

// Size
const sizes = {
    width: window.innerWidth,
    height: window.innerHeight
}

// Scene
const scene = new THREE.Scene()

/**
 * Camera
 */
let camera = new THREE.PerspectiveCamera()

// Base camera
camera = new THREE.PerspectiveCamera(75, sizes.width / sizes.height, 0.01, 1000)
camera.position.set(0, 0, -60)
scene.add(camera)

window.addEventListener('resize', () => {
    // Update sizes
    sizes.width = window.innerWidth
    sizes.height = window.innerHeight

    // Update camera
    camera.aspect = sizes.width / sizes.height
    camera.updateProjectionMatrix();

    // Update renderer
    renderer.setSize(sizes.width, sizes.height)
    renderer.setPixelRatio(Math.min(window.devicePixelRatio, 2))
})



// Controls
const controls = new OrbitControls(camera, canvas)
controls.enableDamping = true
controls.enabled = true

/**
 * Renderer
 */
const renderer = new THREE.WebGLRenderer({
    canvas: canvas
})
renderer.setSize(sizes.width, sizes.height)
renderer.setPixelRatio(Math.min(window.devicePixelRatio, 2))

const renderScene = new RenderPass(scene, camera);

const bloomPass = new UnrealBloomPass(new THREE.Vector2(window.innerWidth, window.innerHeight), 1.5, 0.4, 0.85);
bloomPass.threshold = params.bloomThreshold;
bloomPass.strength = params.bloomStrength;
bloomPass.radius = params.bloomRadius;

const composer = new EffectComposer(renderer);
composer.addPass(renderScene);
composer.addPass(bloomPass);


/*
 * PoolObject
 */
const poolObject = new PoolObject(5000, scene, camera, sizes);

// nombre de d'objet qui rentre dans la boucle d'animation
const numberOfnewObjectAnimated = 4;

// force de la velocité

//Todo rectifier la vitesse en fonction du temps pour avoir quelquechose de plus juste
const strenghVelocity = 1;

/**
 * Animate
 */
const tick = () => {

    //stats
    stats.begin();

    // Update controls
    controls.update()

    //Ajout de rayon à animer
    poolObject.getElementsInAnimationLoop(numberOfnewObjectAnimated);

    //Avancée des rayon non-libre
    poolObject.moveRaysInAnimationLoop(strenghVelocity);

    //Suppression des rayons hors duchamp de vision
    poolObject.setElementsInPool();

    // console.log(poolObject.freeRayNumber)

    //rendu
    // renderer.render(scene, camera)
    composer.render();

    //stats
    stats.end();

    window.requestAnimationFrame(tick)

}

tick()