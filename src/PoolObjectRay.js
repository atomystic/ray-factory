export class PoolObjectRay {

    constructor(data) {

        this.data = data;
        this.free = true;
        this.previousRayObject = null;
        this.nextRayObject = null;

    }

}