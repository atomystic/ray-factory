export class Ray {

    constructor(mesh) {

        this.mesh = mesh;
        this.physicsParameters = { velocity: { x: null, y: null, z: null } }

    }

    //Ajout des paramètres physics
    setPhysicsParameters(physicsParameters) {

        this.physicsParameters = physicsParameters.velocity;

    }


}