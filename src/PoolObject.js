import { PoolObjectRay } from "./PoolObjectRay"
import { Ray } from "./Ray"
import { THREE } from "./Common"

export class PoolObject {

    constructor(poolObjectRaysSize, scene, camera, sizes) {

        //variable des classe
        this.scene = scene;
        this.camera = camera;
        this.sizes = sizes;

        this.creationOriginPoint = { x: 0, y: 0, z: 0 }
        this.rayon = 5;

        this.firstRayObject = null;
        this.lastRayObject = null;
        this.freeRayNumber = 0;
        this.totalRayNumber = 0;
        this.poolObjectRaysSize = poolObjectRaysSize

        this.colors = [new THREE.MeshBasicMaterial({ color: 0xfd6c9e }),
            new THREE.MeshBasicMaterial({ color: 0xafeeee })
        ]


        this.boxGeometry = new THREE.CylinderGeometry(0.03, 0.03, 5, 3);

        //construction du tableau
        this.poolObjectRays = []

        this.createElement(poolObjectRaysSize);

        //initialiser les variables
        this.initialize()

        //Todo redimensionner la table s'il ne reste plus beaucoup d'objet libre,remettre à jour le dernier élément

    }

    initialize() {

        this.firstRayObject = this.poolObjectRays[0];

    }


    createElement(size) {

        for (let i = 0; i < size; i++) {

            //construction de l'objet
            const poolObjectRay = this.buildPoolObjectRay();

            //faire les liens entre les objets précédents et suivants de la
            if (this.totalRayNumber > 0) {

                this.createLinkedBetweenTwoElement(poolObjectRay)

            }

            //setter la valeur du premier et du dernier freeElement
            this.lastRayObject = poolObjectRay

            //setter l'objet au bon endroit
            this.poolObjectRays[this.totalRayNumber] = poolObjectRay

            //incrémenter le nombre d'object dans la liste
            this.freeRayNumber++;
            this.totalRayNumber++;

        }

    }

    getElement() {

        // cherche le dernier objet libre
        const poolObjectRay = this.lastRayObject;

        // mettre l'objet à non-libre
        poolObjectRay.free = false;

        // mettre à jour le dernier objet libre
        this.lastRayObject = poolObjectRay.previousRayObject;

        //effacer tous les liens de l'objet récupéré avec les autres liens
        poolObjectRay.previousRayObject.nextRayObject = null
        poolObjectRay.previousRayObject = null;

        //Donner différentes caractéristiques physique surtout la vélocité/direction
        this.setPhysicProperties(poolObjectRay)

        //Ajouter l'objet à la scene
        this.addRayToTheScene(poolObjectRay);

        this.freeRayNumber--;

    }

    getElementsInAnimationLoop(numberOfnewObjectAnimated) {

        // ajout dans la boucle d'animation d'un certain nombre d'objet 
        for (let i = 0; i < numberOfnewObjectAnimated; i++) {

            this.getElement()

        }

    }

    setElement(poolObjectRay) {

        //mettre l'objet à libre
        poolObjectRay.free = true;

        //supprimer l'objet de la scene
        this.scene.remove(poolObjectRay.data.mesh);

        //recréer les liens avec le premier objet
        this.firstRayObject.previousRayObject = poolObjectRay;
        poolObjectRay.nextRayObject = this.firstRayObject

        //mettre à jour le premier objet libre
        this.firstRayObject = poolObjectRay

        this.freeRayNumber++;

    }

    setElementsInPool() {

        this.poolObjectRays.filter(x => !x.free).forEach((poolObjectRay) => {

            if (this.isNotInFieldOfField(poolObjectRay)) {

                this.setElement(poolObjectRay)
            }

        })

    }

    createLinkedBetweenTwoElement(actualPoolObjectRay) {

        if (this.totalRayNumber > 0) {

            this.lastRayObject.nextRayObject = actualPoolObjectRay;
            actualPoolObjectRay.previousRayObject = this.lastRayObject;

        } else {

            //La toute première itération initialise le dernier object pour que le suivant puisse pointer sur lui
            this.lastRayObject = actualPoolObjectRay

        }

    }

    buildPoolObjectRay() {

        //contruction de la mesh
        const box = new THREE.Mesh(this.boxGeometry, this.getMaterial());

        //construction du PoolObjectRay
        const ray = new Ray(box);
        const poolObjectRay = new PoolObjectRay(ray);

        return poolObjectRay;

    }

    setPhysicProperties(poolObjectRay) {

        //angle sur x et y
        const angle = Math.random() * 2 * Math.PI

        //velocité
        // poolObjectRay.data.physicsParameters.velocity.x = Math.cos(angle) * this.rayon
        // poolObjectRay.data.physicsParameters.velocity.y = Math.sin(angle) * this.rayon

        poolObjectRay.data.physicsParameters.velocity.x = 0
        poolObjectRay.data.physicsParameters.velocity.y = 0


        //le rayon ne part que devant
        poolObjectRay.data.physicsParameters.velocity.z = -1

        //Position
        poolObjectRay.data.mesh.position.x = (Math.cos(angle) * this.rayon)
        poolObjectRay.data.mesh.position.y = (Math.sin(angle) * this.rayon)
        poolObjectRay.data.mesh.position.z = this.creationOriginPoint.z

        //rotation
        poolObjectRay.data.mesh.rotation.x = Math.PI / 2

    }

    moveRaysInAnimationLoop(strenghVelocity) {

        this.poolObjectRays.filter(x => !x.free).forEach(poolObjectRay => {

            let ray = poolObjectRay.data

            //Avancement des rayons
            ray.mesh.position.x += ray.physicsParameters.velocity.x * strenghVelocity
            ray.mesh.position.y += ray.physicsParameters.velocity.y * strenghVelocity
            ray.mesh.position.z += ray.physicsParameters.velocity.z * strenghVelocity

        })
    }

    addRayToTheScene(poolObjectRay) {

        this.scene.add(poolObjectRay.data.mesh)
    }

    isNotInFieldOfField(poolObjectRay) {

        const angleX = Math.atan((poolObjectRay.data.mesh.position.x - this.camera.position.x) / (this.camera.position.z - poolObjectRay.data.mesh.position.z))
            //l'angle de vue est différent en x et en y d'ou la division le ratio de pixel à la fin
        const predicatX = Math.abs(angleX) > ((this.camera.fov / 180) * Math.PI) / (1.8 * this.sizes.x / this.sizes.y)

        const angleY = Math.atan((poolObjectRay.data.mesh.position.y - this.camera.position.y) / (this.camera.position.z - poolObjectRay.data.mesh.position.z))
        const predicatY = Math.abs(angleY) > ((this.camera.fov / 180) * Math.PI) / 1.8

        if (predicatX || predicatY) {

            return true

        }

        return false

    }

    getMaterial() {

        return this.colors[this.getNumberBetween(0, this.colors.length - 1)]

    }

    getNumberBetween(min, max) {

        return Math.floor(((max - min + 1) * Math.random())) + min;

    }

}